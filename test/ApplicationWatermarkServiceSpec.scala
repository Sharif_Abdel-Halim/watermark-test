import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.libs.json.Json
import model._
import play.api.test._
import play.api.test.Helpers._
import org.specs2.matcher.BeEqualTo

/**
 * Intergration test for the watermark webserive
 */
@RunWith(classOf[JUnitRunner])
class ApplicationWatermarkServiceSpec extends Specification {
  
  def createJournal() = {
    Document.create("Journal author", "Journal title")
  }
   
  def createBook() ={
    Document.create("Book author", "Book title",Option(Topic.valueOf("Science")))
  }

  "Application" should {

    "return a BadRequest and an error message for an invalid ticketId when retrieving a status" in new WithApplication {
      val response = route(FakeRequest(GET, "/getStatus/1234")).get
      status(response) must beEqualTo(BAD_REQUEST)
      contentType(response) must beSome("application/json")
      val content = contentAsJson(response)
      (content \ "status").as[String] must beEqualTo("error")
      (content \ "message").as[String] must beEqualTo("unknown ticketId")
    }
    
    "return a BadRequest and an error message for an invalid ticketId when retrieving a document" in new WithApplication {
      val response = route(FakeRequest(GET, "/getDocument/1234")).get
      status(response) must beEqualTo(BAD_REQUEST)
      contentType(response) must beSome("application/json")
      val content = contentAsJson(response)
      (content \ "status").as[String] must beEqualTo("error")
      (content \ "message").as[String] must beEqualTo("unknown ticketId")
    }
  
  
    "return a BadRequest and an errormessage for an invalid json body when creating a watermark" in new WithApplication {
      val response = route(FakeRequest(POST, "/createWatermark").withJsonBody(Json.parse("{\"bla\": \"bla\"}"))).get
      status(response) must beEqualTo(BAD_REQUEST)
      contentType(response) must beSome("application/json")
      val content = contentAsJson(response)
      (content \ "status").as[String] must beEqualTo("error")
      (content \ "message").as[String] must beEqualTo("invalid json")
    }
  
    "return a ticketId when a watermark for a Journal is created" in new WithApplication {
      val response = route(FakeRequest(POST, "/createWatermark").withJsonBody(Json.toJson(createJournal()))).get
      status(response) must beEqualTo(OK)
      val content = contentAsJson(response)
      (content \ "status").as[String] must beEqualTo("success")
      (content \ "id").as[Long] must not beNull
    }
  
    "return a ticketId when a watermark for a book is created" in new WithApplication {
      val response = route(FakeRequest(POST, "/createWatermark").withJsonBody(Json.toJson(createBook()))).get
      status(response) must beEqualTo(OK)
      val content = contentAsJson(response)
      (content \ "status").as[String] must beEqualTo("success")
      (content \ "id").as[Long] must not beNull
    }
   
    "return a finished status after the watermark was created for the Journal" in new WithApplication {
      val response = route(FakeRequest(POST, "/createWatermark").withJsonBody(Json.toJson(createJournal()))).get
      status(response) must beEqualTo(OK)
      val content = contentAsJson(response)
      val ticketId = (content \ "id").as[Long] 
      val statusResponse = route(FakeRequest(GET, "/getStatus/"+ticketId)).get
      val statusContent = contentAsJson(statusResponse)
      (statusContent \ "ticketStatus").as[String] must beEqualTo(Status.Finished.toString())
    }
    
    "return a finished status after the watermark was created for the Book" in new WithApplication {
      val response = route(FakeRequest(POST, "/createWatermark").withJsonBody(Json.toJson(createBook()))).get
      status(response) must beEqualTo(OK)
      val content = contentAsJson(response)
      val ticketId = (content \ "id").as[Long] 
      val statusResponse = route(FakeRequest(GET, "/getStatus/"+ticketId)).get
      val statusContent = contentAsJson(statusResponse)
      (statusContent \ "ticketStatus").as[String] must beEqualTo(Status.Finished.toString())

    }
     
    "return the watermarked Journal after the watermark was created for the Journal" in new WithApplication {
      val response = route(FakeRequest(POST, "/createWatermark").withJsonBody(Json.toJson(createJournal()))).get
      status(response) must beEqualTo(OK)
      val content = contentAsJson(response)
      val ticketId = (content \ "id").as[Long] 
      val documentResponse = route(FakeRequest(GET, "/getDocument/"+ticketId)).get
      val documentContent = contentAsJson(documentResponse)
      val journal = (documentContent \ "document").as[Document] 
      
      // check that the Jorunal has still the same attributes
      journal.author must beEqualTo("Journal author")
      journal.title must beEqualTo("Journal title")
      journal.content must beEqualTo(Content.Journal)
      
      //check watermark values
      val watermark = (documentContent \ "document" \ "watermark").as[Map[String,String]]
      watermark("author") must beEqualTo("Journal author")
      watermark("title") must beEqualTo("Journal title")
      watermark("content") must beEqualTo(Content.Journal.toString())
      
    }
      
    "return the watermarked Book after the watermark was created for the Book" in new WithApplication {
      val response = route(FakeRequest(POST, "/createWatermark").withJsonBody(Json.toJson(createBook()))).get
      status(response) must beEqualTo(OK)
      val content = contentAsJson(response)
      val ticketId = (content \ "id").as[Long] 
      val documentResponse = route(FakeRequest(GET, "/getDocument/"+ticketId)).get
      val documentContent = contentAsJson(documentResponse)
      val book = (documentContent \ "document").as[Document].asInstanceOf[Book]
      
      // check that the Book has still the same attributes
      book.author must beEqualTo("Book author")
      book.title must beEqualTo("Book title")
      book.content must beEqualTo(Content.Book)
      book.topic must beEqualTo(Topic.valueOf("Science"))
      
      //check watermark values
      val watermark = (documentContent \ "document" \ "watermark").as[Map[String,String]]
      watermark("author") must beEqualTo("Book author")
      watermark("title") must beEqualTo("Book title")
      watermark("content") must beEqualTo(Content.Book.toString())
      watermark("topic") must beEqualTo(Topic.valueOf("Science").toString())
      
    }
     
    "return a Bad Request and an error message for an expired ticketId" in new WithApplication {
      val response = route(FakeRequest(POST, "/createWatermark").withJsonBody(Json.toJson(createBook()))).get
      status(response) must beEqualTo(OK)
      val content = contentAsJson(response)
      val ticketId = (content \ "id").as[Long] 
      val documentResponse = route(FakeRequest(GET, "/getDocument/"+ticketId)).get
      status(documentResponse) must beEqualTo(OK)
      val statusResponse = route(FakeRequest(GET, "/getStatus/"+ticketId)).get
      status(statusResponse) must beEqualTo(BAD_REQUEST)
      val statusContent = contentAsJson(statusResponse)
      (statusContent \ "status").as[String] must beEqualTo("error")
      (statusContent \ "message").as[String] must beEqualTo("unknown ticketId")
      
    }
  }
  
}
