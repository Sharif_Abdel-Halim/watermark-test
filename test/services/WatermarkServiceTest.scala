package services

import model._
import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import service.WatermarkService
import org.specs2.matcher.BeEqualTo
/**
 * @author abdel-halim
 */
@RunWith(classOf[JUnitRunner])
class WatermarkServiceTest extends  Specification{

  def createJournal() = {
    Document.create("Clark Kent", "Journal of human flight routes")
  }
   
  def createBook() ={
    Document.create("Bruce Wayne", "The Dark Code",Option(Topic.valueOf("Science")))
  }
   
  "The Watermarkservice" should {
    "put a Jorunal to the map when a ticket is created" in {
      val journal = createJournal()
      val ticketId = WatermarkService.createTicket(journal)
      WatermarkService.getTicketMapEntry(ticketId).document must beEqualTo(journal)
    }
     
    "put a Book to the map when a ticket is created" in {
      val book = createBook()
      val ticketId = WatermarkService.createTicket(book)
      WatermarkService.getTicketMapEntry(ticketId).document must beEqualTo(book)
    }
     
    "throw an Exception when a document with an invalid ticket id is retrieved" in {
      WatermarkService.getDocument(123) must throwA[IllegalArgumentException]
    }
     
    "return a finished status when the watermark of the document is created" in {
      val journal = createJournal()
      val ticketId = WatermarkService.createTicket(journal)
      WatermarkService.createWatermark(ticketId)
      WatermarkService.getTicketMapEntry(ticketId).status must beEqualTo(Status.Finished)
    }
     
    "throw an Exception when a deleted Document is retrieved" in {
      val journal = createJournal()
      val ticketId = WatermarkService.createTicket(journal)
      WatermarkService.createWatermark(ticketId)
      WatermarkService.deleteTicket(ticketId)
      WatermarkService.getDocument(ticketId) must throwA[IllegalArgumentException]
    }
    
    "return a correct watermark when the watermark of a book is created" in {
      val book = createBook()
      val ticketId = WatermarkService.createTicket(book)
      WatermarkService.createWatermark(ticketId)
      val watermark= WatermarkService.getDocument(ticketId).watermark
      watermark("author") must beEqualTo("Bruce Wayne")
      watermark("title") must beEqualTo("The Dark Code")
      watermark("content") must beEqualTo(Content.Book.toString())
      watermark("topic") must beEqualTo(Topic.valueOf("Science").toString())
    }
    "return a correct watermark when the watermark of a Journal is created" in {
       val journal = createJournal()
       val ticketId = WatermarkService.createTicket(journal)
       WatermarkService.createWatermark(ticketId)
       val watermark= WatermarkService.getDocument(ticketId).watermark
       watermark("author") must beEqualTo("Clark Kent")
       watermark("title") must beEqualTo("Journal of human flight routes")
       watermark("content") must beEqualTo(Content.Journal.toString())
     }
   }
}
