package controllers

import play.api.mvc._
import model.Document
import play.api.libs.json._
import service.WatermarkService
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global


class Application extends Controller {

  def retrieveWatermarkedDocument(ticketId: Long) =Action{
    try{
      val document= WatermarkService.getDocument(ticketId)
      
      //documents which are retrieved will be deleted immediately
      Future { WatermarkService.deleteTicket(ticketId) }
      Ok(Json.obj("status" -> "success", "document"->Json.toJson(document)))
      
    }catch{
      case ex:IllegalArgumentException => {
        BadRequest(Json.obj("status" -> "error", "message" -> "unknown ticketId"))
      }
    }
  }
  
  def getStatus(ticketId: Long)=Action{
    val ticketMapEntry= WatermarkService.getTicketMapEntry(ticketId)
    ticketMapEntry match {
      case null => BadRequest(Json.obj("status" -> "error", "message" -> "unknown ticketId"))
      case _ => Ok(Json.obj("status" -> "success", "ticketStatus"->ticketMapEntry.status))
    }
  }
  
  def createWatermarkForDocument = Action(parse.json){ request=>
    request.body.validate[Document].map{
      case (document) =>  {
        val ticketId = WatermarkService.createTicket(document)
        Future{WatermarkService.createWatermark(ticketId)}
        Ok(Json.obj("status" -> "success", "id"->ticketId))
      }
    }.recoverTotal {
      e => BadRequest(Json.obj("status" -> "error", "message" -> "invalid json"))
    }
  }
}
