package model


/**
 * @author abdel-halim
 */
object Status extends Enumeration {
  type Status = Value
  val New, InProcess, Finished = Value
}