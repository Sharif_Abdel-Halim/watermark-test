package model
import play.api.libs.json._




/**
 * @author abdel-halim
 */
sealed trait Topic  
  
case object Business extends Topic
case object Science extends Topic
case object Media extends Topic
  
object Topic{
  
  def valueOf(value: String): Topic = value match {
    case "Business"       => Business
    case "Science"        => Science
    case "Media"          => Media
    case _                => throw new IllegalArgumentException()
  }
  
  implicit val topicReads: Reads[Topic] = __.read[String].map(Topic.valueOf)
  
}