package model

import model.Content.Content
import model.Topic._
import play.api.libs.functional.syntax._
import play.api.libs.json._


/**
 * @author abdel-halim
 */
abstract class Document( val content:Content, val author:String,val title:String) {
  var watermark:Map[String, String] =null
}

case class Book(override val author:String,override val title:String,topic:Topic) extends Document(Content.Book,author,title)
  
case class Journal(override val author:String,override val title:String) extends Document(Content.Journal,author,title)

object Document{
  
  def create(author: String, title: String,topic:Option[Topic]=None): Document = {
     if(topic == None) Journal(author,title)
     else Book(author,title,topic.get)
  }
 
  implicit val reads: Reads[Document] = (
    (JsPath \ "author").read[String] and
    (JsPath \ "title").read[String] and
    (JsPath \ "topic").readNullable[Topic]
  )(Document.create _)
    
  implicit val writes: Writes[Document] = new Writes[Document] {

    override def writes(document: Document) = {
      var json = Json.obj(
      "content" -> document.content.toString,
      "title" -> document.title,
      "author" -> document.author
      )
      
      if(document.isInstanceOf[Book]) json=json ++Json.obj("topic"->document.asInstanceOf[Book].topic.toString())
      //watermark should only be written when it is not null
      if (document.watermark!=null) json = json ++ Json.obj( "watermark"->Json.toJson(document.watermark))
      json
    }
  
  }
}

  
 
  

 
  

