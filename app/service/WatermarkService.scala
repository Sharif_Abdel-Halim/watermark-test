package service
import java.util.concurrent.ConcurrentHashMap
import scala.concurrent.Future
import model.{ TicketMapEntry, Status,Document }
import scala.util.Random
import play.api.libs.json._

/**
 * @author abdel-halim
 */
object WatermarkService {

  private var ticketmap = new ConcurrentHashMap[Long, TicketMapEntry]

  def createTicket(document:Document):Long ={
    val ticketId = Random.nextLong()
    val ticketMapEntry = new TicketMapEntry(Status.New, document)
    ticketmap.putIfAbsent(ticketId, ticketMapEntry)
    ticketId
  }

  def getTicketMapEntry(id: Long): TicketMapEntry = {
    ticketmap.get(id)
  }

  def getDocument(id: Long): Document = {
    val ticketEntry = ticketmap.get(id)
    if (ticketEntry == null) throw new IllegalArgumentException("invalid ticketId")
    else {
      ticketEntry.document
    }
  }

  def deleteTicket(id: Long): Unit = {
    ticketmap.remove(id)
  }
  private def generateWatermarkedDocument(document: Document, ticketId: Long): Document = {
    val watermarkJson = Json.toJson(document)
    document.watermark = watermarkJson.as[Map[String, String]]
    document
  }

  def createWatermark(ticketId: Long): Unit = {
    val ticketMapEntry = ticketmap.get(ticketId)
    setInProcessStatus(ticketId, ticketMapEntry)
    val documentWithWatermark = generateWatermarkedDocument(ticketMapEntry.document, ticketId)
    ticketmap.put(ticketId, ticketMapEntry.copy(status = Status.Finished, document = documentWithWatermark))
  }

  private def setInProcessStatus(ticketId: Long, ticketMapEntry: TicketMapEntry): Unit = {
    ticketmap.put(ticketId, ticketMapEntry.copy(status = Status.InProcess))
  }

}